console.log('Hello from JS');
// INSTRUCTIONS
// TASK 1: Create a function that will prompt the user for 2 numbers and perform different arithmetic operations based on the total of the two numbers:
// a. If the total of the two numbers is less than 10, add the numbers
// b. If the total of the two numbers is 10 - 20, subtract the numbers
// c. If the total of the two numbers is 21 - 29 multiply the numbers
// d. If the total of the two numbers is greater than or equal to 30, divide the numbers
// e. Use an alert for the total of 10 or greater and a console w

// 2. Create a function that will prompt the user for their name and age and print out different alert messages based on the user input:
//   -> If the name OR age is blank/null, print the message are you a time traveler?
//  -> If the name AND age is not blank, print the message with the user’s name and age.




// ACTIVITY
    // TASK 1===================
function firstActivity() {
    let numA = parseInt(prompt('Provide a number'));
    let numB = parseInt(prompt('Provide another number'));
    let sum = numA + numB;
    let difference = numA - numB;
    let product = numA * numB;
    let quotient = numA / numB;
    if ((numA + numB) < 10)  {
        console.warn('The sum of the two numbers is: ' + sum );
    } else if ((numA + numB) <= 20) {
        alert('The difference of the two numbers is: ' + difference );
    } else if ((numA + numB) <= 29) {
        alert('The product of the two numbers is: ' + product );
    } else if ((numA + numB) >= 30) {
        alert('The quotient of the two numbers is: ' + quotient );
    }   
}

// TASK 2======================
function secondActivity() {
    let name = prompt('Enter Name');
    let age = prompt('Enter age');
    if (name === '' || age === '') {
        alert('are you a time traveler?');
    } else {
        alert('Hello ' + name + '. ' + ' Your age is ' + age);
    }
}

// TASK 3: Create a function with switch case statement that will check if the user's age input is within a certain set of expected input:
// - 18 - print the message You are now allowed to party.
// - 21 - print the message You are now part of the adult society.
// - 65 - print the message We thank you for your contribution to society.
// - Any other value - print the message Are you sure you're not an alien?

// TASK 3=======================
function thirdActivity() {
    let ageInput = prompt('What is your age?');
    let age;
    switch (ageInput) {
        case '18':
            age = 'You are now allowed to party.';
            break;
        case '21':
            age = 'You are now part of the adult society.'; 
            break;   
        case '65':
            age = 'We thank you for your contribution to society.';
            break;   
        default:
            age = "Are you sure you're not an alien?"
        }
    return alert(age);    
}


// TRY-CATCH-FINALLY 
// Create a function that wil determine if the age is too old for preschool.
function ageChecker() {
    // We are going to use a tri-catch statement instead

    // get the input of the user.
    // the getElementById() will target a component within the document using its ID attribute.
    // the "document" paramater describes the HTML document/file where the JS module is linked.
    // "value" => describes the value property of our element
    let userInput = document.getElementById('age').value;
    // we will now target the element where we will display the output of this function .
    let message = document.getElementById('outputDisplay');

    try {
        // lets make sure that the input inserted by the user is NOT equals to a blank string.
        // throw -> this statement examines the input and returns an error.
        if (userInput === '') throw 'the input is empty';
        // create a conditional statement that will check if the input is NOT a number
        // in order to check if the value is NOT A NUMBE,we will use a isNaN()
        if (isNaN(userInput)) throw 'the input is Not a Number';
        if (userInput <= 0) throw 'not a valid input';
        if (userInput <= 7) throw 'the input is good for preschool';
        if (userInput > 7) throw 'too old for preschool';
    } catch (err) {
        // the "err" is to define the error that will be thrown by the try section. so "err" is caught by the catch statement and a custom error message will be displayed.
        // how are we going to inject a value inside the html container?
        // => using innerHTML property
        // syntax: element.innerHTML -> this will allow us to get/set the HTML markup contained within the element
        message.innerHTML = "Age Input: " + err;
    } finally {
        // this statement here will be executed regardless of the result above.
        alert('this is from the finally section');
    }
}
console.log('Hello from JS'); //Message

// [SECTION] CONTROL STRUCTURES
// ===========================

// [SUB SECTION] IF-ELSE STATEMENT
// let numA = 3; //we will try to assess the value.

// If statement (Branch)
// The task of the if statement is to execute action if the specified condition is "true".
// if (numA <= 0) {  //truthy branch
//                   //this block of code will run if the condition is MET.  
//     console.log('The condition was Met!');
// }

// let name = 'Lorenzo';

// // create a control structure that will allow the user to proceed if the value maches/passes the condition.
// if (name === 'Lorenzo'){
//     // if the passes the condition the "truethy" branch will run.
//     console.log('User can proceed!');
// }

// [SUB SECTION] 'Else' Statement
// => This executed a statement if ALL other conditions are "FALSE" and/or has failed.

// lets create a control structure that will allow us to check of the user is old enough to drink.
// let age = 21;

// alert() => display a message box to the user which would require their attention.
// if (age >= 18) {
//     // "truthy" branch
//     alert("You're old enough to drink");
// } else {
//     // "falsy" branch
//     alert('Come back another day!');
// }

// lets create a control structure that will ask for the number of drinks that you will order.

// ask the user how many drinks he wants
// let order = prompt('How many orders of drinks do you like?');

// convert the string data type into a number.
// parseInt()=> will allow us to convert strings into integers.

// create a logic that will make sure that the user's input is greater than 0
// in JS there 3 ways to multiply a string.
// 1. repeat() method=> this will allow us to return a new string vlue that contains the numbet of copies of the string.
// syntax: str.repeat(value/number)
// 2. loops => for loop
// 3. loops =>


// if (order > 0) {
//     // console.log(typeof order);
//     let cocktail = '🍸'
//     alert(cocktail.repeat(order)); //multiplication  
// } else {
//     alert('The number should be above 0');
// }

// If you want to create other predetermined conditions you can create a nested (multiple) if-else statement.


// ==================================================
// lets create a control structure that will allow us to simulate a user login.

// prompt box -> prompt(): this will allow us to display a prompty dialog box which we can the user for an input.
// syntax: prompt(text/message[REQUIRED], placeholder/default text[OPTIONAL]);
// prompt("Please enter your first name:", "Anthony");
// ================================================


// MINI ACTIVITY - vaccine checker
function vaccineChecker(){
    // ask information from the user.
    let vax = prompt('What brand is your vaccine?');
    // pfier, PFIZER
    // so we need to process the input of the user so that whatever input he will enter we can control the uniformity of the character casing.
    // toLowerCase() -> will allow us to convert a string into all lowercase characters. (syntax: string.toLowerCase())
    vax = vax.toLowerCase();
    // create a logic that will allow us to check the value inserted by the user to match the given parameters.
    if (vax === 'pfizer' ||  vax === 'moderna' || vax === 'astrazenica' || vax === 'janssen') {
        // display the response back to the client.
        alert(vax + ' is allowed to travel');
    } else {
        alert('Sorry not permitted');
    }
}
// for this mini task, we want the user to be able to invoke this function with the user of a trigger.
// onClick => is an example of JS Event. This "event" in JS occurs when the user clicks on an element/component to trigger a certain procedure.
// syntax: <element/component onclick="myScript/Function" 

// Typhoon Checker
function determineTyphoonIntensity (){
    // going to need an input from the user.
    // we need a number data type so that we can properly compare the values.
    let windspeed = parseInt(prompt('Wind Speed: '));
    console.log(typeof windspeed); //this is to prove that we can directly pass the value of the variable and feed to the parseInt method.
    
    // for this logic/structure, it will only assess the whole number.
    // note: "else if" is 2 words
    if (windspeed <= 29) {
        alert('Not a typhoon yet.');
    } else if (windspeed <= 61) {
        alert('Tropical Depression Detected');
        // this will run if the 2ns statement was met.
    } else if (windspeed <= 88) {
        alert('Tropical Storm Detected!');
    } else if (windspeed <= 117) {
        alert('Severe Tropical Storm Detected!');
    }
    else {
        alert('Typhoon detected!');
    }
}

// ===========================================
// CONDITIONAL TERNARY
// it still follows the same syntax with an if-else
// need (truthy, falsy)
// this is the only JS operator that takes 3 operands
// ? question mark => this would describe a condition that if resulted to "true" will run "truthy"
// : colon => this symbol will separate the "truthy" and the "falsy" statements
// syntax: condition ? "truthy" : "falsy"

// AGE CHECKER
function ageChecker() {
    let age = parseInt(prompt('How old are you?'));
    // lets simplify our structure below with the help of a ternary operator
    return (age >= 18) ? alert('Old enough to vote') : alert('Not yet old enough');
    // if (age >= 18) {
    //     // truthy
    //     alert('Old enough to vote');
    // } else {
    //     // falsy
    //     alert('Not yet old enough');
    // }  
}
// ==================================================

// create a function that will determine the owner of a computer unit.

function determineComputerOwner() {
    let unit = prompt('What is the unit no.?');
    let manggagamit;
    // this unit ->represents the unit number.
    // unit === case
    // the manggagamit -> represents the user who owns the unit.
    switch (unit) {
        // declare multiple cases to represent each outcome that will match the value inside the expression.
        case '1':
            manggagamit = 'John Travolta';
            break;
        case '2':
            manggagamit = 'Steve Jobs'; 
            break;   
        case '3':
            manggagamit = 'Sid Meier';
            break;
        case '4':
            manggagamit = 'Onel de Guzman';
            break;
        case '5':
            manggagamit = 'Christian Salvador';
            break;    
        default:
            manggagamit = 'wala yan sa options na pagpipilian' //if all else fails or if none of the preceeding cases above meets the expression, this statement will become the fail safe/default response.
    }
    return alert(manggagamit);
}
    
// When to use "" (double quotes) OVER '' (single quotes)
// => "name" === 'name'
// we can use either methods to declare a strin value . however we can use one over the other to ESCAPE the scope of the inital symbol.
// let dialog = "'Drink your water bhie' - mimiyuhhh says";
// let dialog2 = '"I shall return" - Mcarthur';
// ownership = "Aling Nena\'s Tindahan"; //alternative in inserting apostrophe.

// passed down determineComputerOwner(); into its button component in the page

//create a demo video on the 4 functions that we have created

// deadline: Monday (5:45pm)


 